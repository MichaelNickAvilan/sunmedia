var MainView = {
    /**
     * The init method
     */
    init:function(){
        var data = MainView.getData();
        MainView.renderValue('rgb_td',data.rgb);
        MainView.renderValue('wb_td',data.wb);
        MainView.renderValue('colors_td',data.colors);
    },
    /**
     * Renders an object value into a DOM container
     * @param {string} id - The ID of a container DOM element 
     * @param {*} value - An object the be rendered
     */
    renderValue:function(id, value){
        var el = document.getElementById(id);
        el.innerHTML = JSON.stringify(value);
    },
    /**
     * Returns the original test data into an object
     */
    getData:function(){
        var rgb = {
            red: "#FF0000",
            green: "#00FF00",
            blue: "#0000FF"
        };
        
        var wb = {
            white: "#FFFFFF",
            black: "#000000"
        };
        var colors = {};
        colors = MainView.assign(colors, wb);
        colors = MainView.assign(colors, rgb);
        return { rgb:rgb, wb:wb, colors:colors };
    },
    /**
     * Returns the target object with the properties of the source object
     * @param {*} target - The target object to assign the properties 
     * @param {*} source - The source object to take the properties
     */
    assign:function(target, source){
        var values = Object.keys(source);
        for(var i=0;i<values.length;i++){
            target[values[i]] = source[values[i]];
        }
        return target;
    }
}
MainView.init();