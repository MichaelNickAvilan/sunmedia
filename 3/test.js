var el  = document.getElementById('textarea_input');
var promise = new Promise(function(resolve, reject) {
    setTimeout(function() {
        if (Math.round(Math.random()) === 1) {
            resolve("Success!");
        } else {
            reject("Fail!");
        }
    }, 1000);
});

promise.then(function(result) {
    el.value = result;
}, function(error) {
    el.value = error;
});