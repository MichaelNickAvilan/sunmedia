var MainView = {
    a_counter:1,
    a_interval:null,
    textarea:null,
    
    /**
     * The init method
     */
    init:function(){
        MainView.textarea = document.getElementById('textarea_input');
        MainView.a_interval = setInterval(MainView.intervalExcecution, 1000)
    },
    /**
     * The interval execution method
     */
    intervalExcecution:function(){
        if(MainView.a_counter<=5){
            MainView.textarea.value+='Iteracion: '+MainView.a_counter+' - ';
            MainView.a_counter++;
        }else{
            clearInterval(MainView.a_interval);
        }
    }
}
MainView.init();
