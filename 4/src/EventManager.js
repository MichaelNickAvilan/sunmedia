export default class EventManager{
    constructor(events, types) {
        this.events = events;
        this.types = types;
    }
    
    run() {
        console.log("RUNNING");
        const Interval = {
            a_counter:0,
            total_secconds:0,
            events:[],
            init(events, total_secconds){
                Interval.events = events;
                Interval.total_secconds = total_secconds;
                Interval.a_interval = setInterval(Interval.intervalExcecution, 1000)
            },
            intervalExcecution:function(){
                if(Interval.a_counter<=Interval.total_secconds){
                    Interval.a_counter++;
                    for(let i=0;i<Interval.events.length;i++){
                        if(Interval.a_counter===Interval.events[i].second){
                            console.log(Interval.events[i]);
                        }                    
                    }
                }else{
                    clearInterval(Interval.a_interval);
                }
            }

        }
        Interval.init(this.events, this.getTotalSecconds());
        
    }
    getTotalSecconds(){
        let secconds = 0;
        for(let i=0;i<this.events.length;i++){
            secconds+=this.events[i].second;
        }
        return secconds;
    }
};